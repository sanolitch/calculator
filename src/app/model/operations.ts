import { INumberItem, NumberItem } from './number';
import { Action, IAction } from './action';

export interface IOperation extends IAction {

    Execute(action: NumberItem): number;

}

export abstract class Operation extends Action implements IOperation {

    public Clone(): INumberItem {
        // @ts-ignore
        return new this.constructor(this.stringValue);
    }

    public Execute(action: INumberItem): number {
        return 0;
    }

    public ToString(): string {
        return this.stringValue;
    }

}


