import { ILinkedListItem } from './input-list';
import { IOperation } from './operations';
import { Action, ACTION_TYPES, IAction } from './action';
import { RegularOperation } from './regular-operations';

export interface INumberItem extends IAction, ILinkedListItem<INumberItem> {

    instantOperation: IOperation;
    operation: RegularOperation;

    isPointed: boolean;

    value: number;

    Value: number;
    Execute(): number;
    AddSymbol(value: number): void;
    AddSymbol(value: string): void;
    AddSymbol(value: any): void;

}

export class NumberItem extends Action implements INumberItem {

    prev: INumberItem;
    next: INumberItem;

    instantOperation: IOperation;
    operation: RegularOperation;

    isPointed: boolean;

    value: number;

    constructor(value: string) {
        super();

        this.type = ACTION_TYPES.Number;

        this.stringValue = value;
        this.value = +value;
    }

    get Value(): number {
        if (this.instantOperation) {
            const value = this.instantOperation.Execute(this);

            this.instantOperation = null;

            return value;
        }
        return this.value;
    }

    public Execute(): number {
        if (!this.operation) {
            return this.Value;
        }
        const result = this.operation.Execute(this);

        if (this.next) {
            this.operation = this.next.operation;
            this.value = result;
            this.next.operation = null;
            this.next = this.next.next;
        }
        return result;
    }

    public AddSymbol(value: number): void;
    public AddSymbol(value: string): void;

    public AddSymbol(value: any): void {
        const strValue =
            this.value.toString() +
            (this.isPointed ?  '.' : '') +
                value.toString()

        this.value = +strValue;

        if (this.isPointed) {
            this.isPointed = false;
        }
    }

    public ToString(): string {
        return (this.instantOperation ? this.instantOperation.ToString() : '') +
            this.value +
            (this.isPointed ?  ',' : '') +
            (this.operation ? this.operation.ToString() : '');
    }

}

