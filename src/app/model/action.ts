import { IOperation } from './operations';
import { INumberItem } from './number';
import { Button, IButton } from './button';

export const enum ACTION_TYPES {

    Operation,
    Number,
    InstantOperation,
    Symbol

}

export interface IAction {

    type: ACTION_TYPES;
    stringValue: string;

    Value: number | string;
    ToString(): string;
    Clone(): INumberItem | IOperation;

}

export abstract class Action implements IAction {

    type: ACTION_TYPES;

    stringValue: string;

    get Value(): number | string {
        return this.stringValue;
    }

    public Clone(): INumberItem | IOperation {
        // @ts-ignore
        return new this.constructor(this.stringValue);
    }

    public ToString(): string {
        return this.Value.toString();
    }

    public CreateButton(): IButton {
        return new Button(this.stringValue, this.Clone());
    }
}
