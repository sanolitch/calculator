import { INumberItem } from './number';
import { NumberStack } from './stack';
import { IOperation } from './operations';
import { RegularOperation } from './regular-operations';
import { SymbolItem } from './symbol-operations';

export interface ILinkedListItem<T extends ILinkedListItem<T>> {

    next: T;

}

export class LinkedList<T extends ILinkedListItem<T>> {

    protected linkedList: T[];

    constructor() {
        this.linkedList = [];
    }

    get Count(): number {
        return this.linkedList.length;
    }

    get IsEmpty(): boolean {
        return this.linkedList.length === 0;
    }

    public Peek(): T {
        return this.IsEmpty ?
            null :
            this.linkedList[this.linkedList.length - 1];
    }

    public Push(item: T): void {
        if (!this.IsEmpty) {
            this.Peek().next = item;
        }
        this.linkedList.push(item);
    }

}

export class InputList extends LinkedList<INumberItem> {

    public Push(numberItem: INumberItem): void {
        const lastAdded = this.Peek();
        const isPreviousNumberNotFinished = lastAdded && !lastAdded.operation

        if (isPreviousNumberNotFinished) {
            return lastAdded.AddSymbol(numberItem.Value);
        }
        super.Push(numberItem);
    }

    public SetOperation(operation: RegularOperation): void {
        const lastAdded = this.Peek();

        if (!lastAdded) {
            return;
        }
        lastAdded.operation = operation;
    }

    public SetInstantOperation(operation: IOperation): void {
        const lastAdded = this.Peek();

        if (!lastAdded) {
            return;
        }
        lastAdded.instantOperation = operation;
    }

    public AddPoint(lastAdded: INumberItem): void {
        lastAdded.isPointed = true;
    }

    public AddSymbol(symbolOperation: SymbolItem): void {
        const lastAdded = this.Peek();

        if (!lastAdded) {
            return;
        }
        if (symbolOperation.Value === ',') {
            return this.AddPoint(lastAdded);
        }
        lastAdded.AddSymbol(symbolOperation.Value);
    }

    public ToString(): string {
        return this.linkedList
            .map(item => item.ToString())
            .join('');
    }

    public Clear(): void {
        this.linkedList = [];
    }

    public ExecuteActions(): number {
        if (this.Count === 1) {
            return this.Peek().Value;
        }
        const stack = new NumberStack();
        let lastItem: INumberItem;

        this.linkedList.map(item => stack.Push(item));

        while (!stack.IsEmpty) {
            const item = stack.Pop();

            item.Execute();
            stack.Push(item)

            lastItem = item;
        }
        return lastItem.value;
    }

}
