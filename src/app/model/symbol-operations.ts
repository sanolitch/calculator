import { Action, ACTION_TYPES } from './action';

export class SymbolItem extends Action {

    stringValue: string;
    type: ACTION_TYPES;

    constructor(value: string) {
        super();

        this.stringValue = value;
        this.type = ACTION_TYPES.Symbol;
    }

    get Value(): string {
        return this.stringValue;
    }

}

