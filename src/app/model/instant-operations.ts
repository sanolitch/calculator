import { ACTION_TYPES } from './action';
import { Operation } from './operations';
import { INumberItem } from './number';

abstract class InstantOperation extends Operation {

    stringValue: string;
    type: ACTION_TYPES;

    protected constructor() {
        super();

        this.type = ACTION_TYPES.InstantOperation;
    }

}

export class SqrtOperation extends InstantOperation {

    constructor() {
        super();

        this.stringValue = '√';
    }

    public Execute(action: INumberItem): number {
        return Math.sqrt(action.value);
    }

}

export class PercentOperation extends InstantOperation {

    constructor() {
        super();

        this.stringValue = '%';
    }

    public Execute(action: INumberItem): number {
        return action.value / 100;
    }

}
