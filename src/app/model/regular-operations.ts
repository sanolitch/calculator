import { INumberItem } from './number';
import { ACTION_TYPES } from './action';
import { Operation } from './operations';

export abstract class RegularOperation extends Operation {

    isHighPriority: boolean;

    protected constructor() {
        super();

        this.type = ACTION_TYPES.Operation;
    }

}

export class SumOperation extends RegularOperation {

    constructor() {
        super();

        this.stringValue = '+';
    }

    public Execute(action: INumberItem): number {
        const leftSide = action.Value;
        const rightSide = action.next.Value;

        return leftSide + rightSide;
    }

}

export class MinusOperation extends RegularOperation {

    constructor() {
        super();

        this.stringValue = '-';
    }

    public Execute(action: INumberItem): number {
        const leftSide = action.Value;
        const rightSide = action.next.Value;

        return leftSide - rightSide;
    }

}

export class MultiplyOperation extends RegularOperation {

    constructor() {
        super();

        this.stringValue = 'x';
        this.isHighPriority = true;
    }

    public Execute(action: INumberItem): number {
        const leftSide = action.Value;
        const rightSide = action.next.Value;

        return leftSide * rightSide;
    }

}

export class DivideOperation extends RegularOperation {

    constructor() {
        super();

        this.stringValue = '/';
        this.isHighPriority = true;
    }

    public Execute(action: INumberItem): number {
        const leftSide = action.Value;
        const rightSide = action.next.Value;

        return leftSide / rightSide;
    }

}
