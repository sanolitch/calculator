import { INumberItem } from './number';

export class Stack<T> {

    protected readonly _items: T[];

    constructor() {
        this._items = [];
    }

    get Count(): number {
        return this._items.length;
    }

    get IsEmpty(): boolean {
        return this.Count === 0;
    }

    public Push(item: T): void {
        this._items.push(item);
    }

    public Pop(): T {
        return this._items.pop();
    }

    public Peek(): T {
        return this.IsEmpty ? null : this._items[this.Count - 1];
    }

}

export class NumberStack extends Stack<INumberItem> {

    Push(item: INumberItem): void {
        const isHighPriority = item.operation && item.operation.isHighPriority;
        const isPreviousHighPriority =
            this.IsItemPartOfHighPriorityOperationChain(item);

        if (isHighPriority && !isPreviousHighPriority) {
            super.Push(item)
        }
        if (item.operation && !item.operation.isHighPriority) {
            this.AddItemInTheEnd(item);
        }
    }

    private CheckActionChain(lastItem: INumberItem, newItem: INumberItem): boolean {
        let nextItemInChain = lastItem.next;

        while (nextItemInChain.operation && nextItemInChain.operation.isHighPriority || !nextItemInChain) {
            if (nextItemInChain === newItem) {
                return true;
            }
            nextItemInChain = nextItemInChain.next;
        }
        return false;
    }

    private IsItemPartOfHighPriorityOperationChain(newItem: INumberItem): boolean {
        const lastItem = this.Peek();

        return !this.IsEmpty &&
            lastItem &&
            lastItem.operation &&
            lastItem.operation.isHighPriority &&
            this.CheckActionChain(lastItem, newItem);
    }

    private AddItemInTheEnd(item: INumberItem): void {
        this._items.unshift(item);
    }

}
