import { IOperation } from './operations';
import { INumberItem } from './number';

export interface IButton {

    action: INumberItem | IOperation;

    ToString(): string;
}

export class Button implements IButton {

    action: INumberItem | IOperation;

    shortcut: string;

    constructor(stringValue: string,
                action: INumberItem | IOperation,
                shortcut: string = null) {
        this.action = action;
        this.shortcut = shortcut ? shortcut : stringValue;
    }

    public ToString(): string {
        return this.action.stringValue;
    }

}
