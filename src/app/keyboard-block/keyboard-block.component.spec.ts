import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { KeyboardBlockComponent } from './keyboard-block.component';

describe('KeyboardBlockComponent', () => {
    let component: KeyboardBlockComponent;
    let fixture: ComponentFixture<KeyboardBlockComponent>;

    beforeEach(async(() => {
        void TestBed.configureTestingModule({
            declarations: [ KeyboardBlockComponent ]
        })
            .compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(KeyboardBlockComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should create', () => {
        void expect(component).toBeTruthy();
    });
});
