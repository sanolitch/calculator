import { Component, OnInit } from '@angular/core';

import { CalcService } from '../services/calc.service';
import { InputService } from '../services/input.service';

import { IButton } from '../model/button';

@Component({
    selector: 'app-keyboard-block',
    templateUrl: './keyboard-block.component.html',
    styleUrls: ['./keyboard-block.component.scss']
})
export class KeyboardBlockComponent implements OnInit {

    result = {
        ToString: () => {
            return '=';
        }
    }
    cancel = {
        ToString: () => {
            return 'C';
        }
    }

    constructor(private inputService: InputService,
                private calcService: CalcService) {
    }

    get Buttons(): IButton[][] {
        return this.inputService.Buttons;
    }

    public ngOnInit(): void {
    }

    public Execute(): void {
        this.calcService.Calculate();
    }

    public Reset(): void {
        this.inputService.Clear();
        this.calcService.Clear();
    }
}
