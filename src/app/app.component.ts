import { Component, HostListener } from '@angular/core';

import { InputService } from './services/input.service';
import { CalcService } from './services/calc.service';

@Component({
    selector: 'app-root',
    templateUrl: './app.component.html',
    styleUrls: ['./app.component.scss']
})
export class AppComponent {
    title = 'calculator';

    constructor(private inputService: InputService,
                private calcService: CalcService) {
    }

    @HostListener('document:keydown', ['$event'])
    private HandleKeyboardEvent(event: KeyboardEvent): void {
        const key = event.key;

        switch (key) {
            case 'Enter': {
                this.calcService.Calculate();
                break;
            }
            case 'Escape': {
                this.inputService.Clear();
                break;
            }
            default: {
                this.inputService.OnKeyPressed(key);
                break;
            }
        }
    }
}
