import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CalculatorMainComponent } from './calculator-main.component';

describe('CalculatorMainComponent', () => {
    let component: CalculatorMainComponent;
    let fixture: ComponentFixture<CalculatorMainComponent>;

    beforeEach(async(() => {
        void TestBed.configureTestingModule({
            declarations: [ CalculatorMainComponent ]
        })
            .compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(CalculatorMainComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should create', () => {
        void expect(component).toBeTruthy();
    });
});
