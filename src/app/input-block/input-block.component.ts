import { Component, OnInit } from '@angular/core';
import { Subject } from 'rxjs';

import { InputService } from '../services/input.service';
import { CalcService } from '../services/calc.service';
import { HistoryService } from '../services/history-service';

@Component({
    selector: 'app-input-block',
    templateUrl: './input-block.component.html',
    styleUrls: ['./input-block.component.scss']
})
export class InputBlockComponent implements OnInit {

    constructor(private inputService: InputService,
                private calcService: CalcService,
                private historyService: HistoryService) { }

    get InputValue(): string {
        return this.inputService.InputString;
    }

    get Result(): Subject<number> {
        return this.calcService.Result;
    }

    get History(): Subject<string> {
        return this.historyService.History;
    }

    public ngOnInit(): void {
    }

}
