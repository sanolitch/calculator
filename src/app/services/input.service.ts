import { Injectable } from '@angular/core';

import { InputList } from '../model/input-list';
import { INumberItem, NumberItem } from '../model/number';
import { IOperation } from '../model/operations';
import { IButton } from '../model/button';
import { ACTION_TYPES } from '../model/action';
import {
    DivideOperation,
    MinusOperation,
    MultiplyOperation,
    RegularOperation,
    SumOperation,
} from '../model/regular-operations';
import { PercentOperation, SqrtOperation } from '../model/instant-operations';
import { SymbolItem } from '../model/symbol-operations';

@Injectable({providedIn: 'root'})
export class InputService {

    private readonly _inputList: InputList;

    private _buttons: IButton[][];
    private _cachedButtons: IButton[];

    constructor() {
        this._inputList = new InputList();

        this.InitActions();
    }

    get InputString(): string {
        return this._inputList.ToString();
    }

    get Buttons(): IButton[][] {
        return this._buttons;
    }

    get InputList(): InputList {
        return this._inputList;
    }

    public Add(action: INumberItem | IOperation | SymbolItem): void {
        switch (action.type) {
            case ACTION_TYPES.Number: {
                return this._inputList.Push(action as INumberItem);
            }
            case ACTION_TYPES.Operation: {
                return this._inputList.SetOperation(action as RegularOperation);
            }
            case ACTION_TYPES.InstantOperation: {
                return this._inputList.SetInstantOperation(action as IOperation);
            }
            case ACTION_TYPES.Symbol: {
                return this._inputList.AddSymbol(action as SymbolItem)
            }
        }
    }

    public Clear(): void {
        this._inputList.Clear();
    }

    public OnKeyPressed(key: string): void {
        const commands = this._cachedButtons ? this._cachedButtons : [].concat(...this._buttons);
        const suitableCommand = commands.find(command => command.shortcut === key);

        if (suitableCommand) {
            this.Add(suitableCommand.action.Clone());
        }
    }

    private InitActions(): void {
        this._buttons = [
            [
                new NumberItem('7').CreateButton(),
                new NumberItem('4').CreateButton(),
                new NumberItem('1').CreateButton(),
                new SymbolItem('00').CreateButton(),
            ],
            [
                new SqrtOperation().CreateButton(),
                new NumberItem('8').CreateButton(),
                new NumberItem('5').CreateButton(),
                new NumberItem('2').CreateButton(),
                new NumberItem('0').CreateButton(),
            ],
            [
                new PercentOperation().CreateButton(),
                new NumberItem('9').CreateButton(),
                new NumberItem('6').CreateButton(),
                new NumberItem('3').CreateButton(),
                new SymbolItem(',').CreateButton(),
            ],
            [
                new DivideOperation().CreateButton(),
                new MultiplyOperation().CreateButton(),
                new MinusOperation().CreateButton(),
                new SumOperation().CreateButton(),
            ],
        ];
    }

}
