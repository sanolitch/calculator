import { CalcService } from '../calc.service';
import { InputService } from '../input.service';
import { HistoryService } from '../history-service';
import { NumberItem } from '../../model/number';
import { DivideOperation, MinusOperation, MultiplyOperation, SumOperation } from '../../model/regular-operations';
import { PercentOperation, SqrtOperation } from '../../model/instant-operations';

describe('CalcService', () => {
   let calcService: CalcService;
   let inputService: InputService;
   let historyService: HistoryService;

   beforeEach(
       () => {
           inputService = new InputService();
           historyService = new HistoryService();
           calcService = new CalcService(inputService, historyService);
       }
   );
   it('Sum Operation, expected 4', () => {
       inputService.Add(new NumberItem('1'));
       inputService.Add(new SumOperation());
       inputService.Add(new NumberItem('3'));

       const result = inputService.InputList.ExecuteActions();

       expect(result).toBe(4);
   })
   it('Multiply Operation, expected 6', () => {
       inputService.Add(new NumberItem('2'));
       inputService.Add(new MultiplyOperation());
       inputService.Add(new NumberItem('3'));

       const result = inputService.InputList.ExecuteActions();

       expect(result).toBe(6);
   })
   it('Minus Operation, expected -1', () => {
       inputService.Add(new NumberItem('2'));
       inputService.Add(new MinusOperation());
       inputService.Add(new NumberItem('3'));

       const result = inputService.InputList.ExecuteActions();

       expect(result).toBe(-1);
   })
   it('Divide Operation, expected 3', () => {
       inputService.Add(new NumberItem('6'));
       inputService.Add(new DivideOperation());
       inputService.Add(new NumberItem('2'));

       const result = inputService.InputList.ExecuteActions();

       expect(result).toBe(3);
   })
   it('Percent Operation, expected 0.03', () => {
       inputService.Add(new NumberItem('3'));
       inputService.Add(new PercentOperation());

       const result = inputService.InputList.ExecuteActions();

       expect(result).toBe(0.03);
   })
   it('Sqrt Operation, expected 5', () => {
       inputService.Add(new NumberItem('25'));
       inputService.Add(new SqrtOperation());

       const result = inputService.InputList.ExecuteActions();

       expect(result).toBe(5);
   })
   it('Floating Point Operation, expected 6.3', () => {
       const a = new NumberItem('4');

       a.isPointed = true;

       inputService.Add(a);
       inputService.Add(new NumberItem('2'));
       inputService.Add(new SumOperation());

       const b = new NumberItem('2');

       b.isPointed = true;

       inputService.Add(b);
       inputService.Add(new NumberItem('1'));

       const result = inputService.InputList.ExecuteActions();

       expect(result).toBeCloseTo(6.3);
   })
   it('Complex Operation, expected -1', () => {
       // 5 + 3 * 6 / 3 - 2 * 6
       inputService.Add(new NumberItem('25'));
       inputService.Add(new SqrtOperation());
       inputService.Add(new SumOperation());

       inputService.Add(new NumberItem('3'));
       inputService.Add(new MultiplyOperation());

       inputService.Add(new NumberItem('6'));
       inputService.Add(new DivideOperation());

       inputService.Add(new NumberItem('9'));
       inputService.Add(new SqrtOperation());
       inputService.Add(new MinusOperation());

       inputService.Add(new NumberItem('200'));
       inputService.Add(new PercentOperation());
       inputService.Add(new MultiplyOperation());

       inputService.Add(new NumberItem('6'));


       const result = inputService.InputList.ExecuteActions();

       expect(result).toBe(-1);
   })
});
