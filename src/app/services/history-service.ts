import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';

@Injectable({providedIn: 'root'})
export class HistoryService {

    private readonly _history: Subject<string>;

    constructor() {
        this._history = new Subject<string>();
    }

    get History(): Subject<string> {
        return this._history;
    }

    public AddHistoryValue(value: string): void {
        this._history.next(value);
    }

}
