import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';

import { InputService } from './input.service';
import { HistoryService } from './history-service';

@Injectable({ providedIn: 'root' })
export class CalcService {

    private readonly _resultSubject: BehaviorSubject<number>;

    constructor(private inputService: InputService, private historyService: HistoryService) {
        this._resultSubject = new BehaviorSubject<number>(0);
    }

    get Result(): BehaviorSubject<number> {
        return this._resultSubject;
    }

    public Calculate(): void {
        const historyString = this.inputService.InputList.ToString();
        const actionsList = this.inputService.InputList;
        const result = actionsList.ExecuteActions();

        this._resultSubject.next(result);
        this.historyService
            .AddHistoryValue(`${historyString} = ${result}`);
        this.inputService.Clear();
    }

    public Clear(): void {
        this._resultSubject.next(0);
    }
}
