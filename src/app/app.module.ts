import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { CalcService } from './services/calc.service';
import { InputService } from './services/input.service';
import { HistoryService } from './services/history-service';

import { AppComponent } from './app.component';
import { CalculatorMainComponent } from './calculator-main/calculator-main.component';
import { InputBlockComponent } from './input-block/input-block.component';
import { KeyboardBlockComponent } from './keyboard-block/keyboard-block.component';
import { KeyboardButtonComponent } from './keyboard-button/keyboard-button.component';

@NgModule({
    declarations: [
        AppComponent,
        CalculatorMainComponent,
        InputBlockComponent,
        KeyboardBlockComponent,
        KeyboardButtonComponent
    ],
    imports: [
        BrowserModule
    ],
    providers: [InputService, CalcService, HistoryService],
    bootstrap: [AppComponent]
})
export class AppModule {
}
