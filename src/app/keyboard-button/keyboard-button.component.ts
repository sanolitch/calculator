import { Component, Input, OnInit } from '@angular/core';

import { InputService } from '../services/input.service';
import { IButton } from '../model/button';

@Component({
    selector: 'app-keyboard-button',
    templateUrl: './keyboard-button.component.html',
    styleUrls: ['./keyboard-button.component.scss']
})
export class KeyboardButtonComponent implements OnInit {

    @Input() button: IButton;

    constructor(private inputService: InputService) {
    }

    get Text(): string {
        if (!this.button) {
            return '';
        }
        return this.button.ToString();
    }

    public ngOnInit(): void {
    }

    public OnButtonClick(): void {
        if (!this.button.action) {
            return;
        }
        this.inputService.Add(this.button.action.Clone());
    }

}
