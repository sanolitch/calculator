import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { KeyboardButtonComponent } from './keyboard-button.component';

describe('KeyboardButtonComponent', () => {
    let component: KeyboardButtonComponent;
    let fixture: ComponentFixture<KeyboardButtonComponent>;

    beforeEach(async(() => {
        void TestBed.configureTestingModule({
            declarations: [ KeyboardButtonComponent ]
        })
            .compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(KeyboardButtonComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should create', () => {
        void expect(component).toBeTruthy();
    });
});
